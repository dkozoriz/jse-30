package ru.t1.dkozoriz.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.api.repository.IUserRepository;
import ru.t1.dkozoriz.tm.api.repository.business.IProjectRepository;
import ru.t1.dkozoriz.tm.api.repository.business.ITaskRepository;
import ru.t1.dkozoriz.tm.api.service.IPropertyService;
import ru.t1.dkozoriz.tm.api.service.IUserService;
import ru.t1.dkozoriz.tm.enumerated.Role;
import ru.t1.dkozoriz.tm.exception.entity.EntityException;
import ru.t1.dkozoriz.tm.exception.field.*;
import ru.t1.dkozoriz.tm.exception.user.LoginEmptyException;
import ru.t1.dkozoriz.tm.exception.user.LoginIsExistException;
import ru.t1.dkozoriz.tm.exception.user.PasswordEmptyException;
import ru.t1.dkozoriz.tm.model.User;
import ru.t1.dkozoriz.tm.util.HashUtil;

public final class UserService extends AbstractService<User, IUserRepository> implements IUserService {

    private final static String NAME = "User";

    @NotNull
    private final IProjectRepository projectRepository;

    @NotNull
    private final ITaskRepository taskRepository;

    @NotNull
    private final IPropertyService propertyService;

    public UserService(
            @NotNull final IUserRepository repository,
            @NotNull final IProjectRepository projectRepository,
            @NotNull final ITaskRepository taskRepository,
            @NotNull final IPropertyService propertyService
    ) {
        super(repository);
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
        this.propertyService = propertyService;
    }

    @NotNull
    @Override
    protected String getName() {
        return NAME;
    }

    @NotNull
    public User create(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new LoginIsExistException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(Role.USUAL);
        return repository.add(user);
    }

    @NotNull
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final String email) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new LoginIsExistException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (isEmailExist(login)) throw new EmailIsExistException();
        @NotNull final User user = create(login, password);
        user.setEmail(email);
        return user;
    }

    @NotNull
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final Role role) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new LoginIsExistException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (role == null) throw new RoleEmptyException();
        @NotNull final User user = create(login, password);
        user.setRole(role);
        return user;
    }


    @Nullable
    public User findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        return repository.findByLogin(login);
    }

    @Nullable
    public User findByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        return repository.findByEmail(email);
    }

    @Nullable
    public User removeOne(@Nullable final User model) {
        if (model == null) return null;
        @Nullable final User user = super.remove(model);
        if (user == null) return null;
        @NotNull final String userId = user.getId();
        taskRepository.clear(userId);
        projectRepository.clear(userId);
        return user;
    }

    @Nullable
    public User removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final User user = findByLogin(login);
        if (user == null) throw new EntityException(getName());
        return removeOne(user);
    }

    @Nullable
    public User removeByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @Nullable final User user = findByEmail(email);
        if (user == null) throw new EntityException(getName());
        return remove(user);
    }

    @NotNull
    public User setPassword(@Nullable final String id, @Nullable final String password) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final User user = findById(id);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        return user;
    }

    @NotNull
    public User updateUser(@Nullable final String id,
                           @Nullable final String firstName,
                           @Nullable final String lastName,
                           @Nullable final String middleName
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final User user = findById(id);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        return user;
    }

    @NotNull
    public Boolean isLoginExist(@Nullable final String login) {
        if (login == null || login.isEmpty()) return false;
        return repository.isLoginExist(login);
    }

    @NotNull
    public Boolean isEmailExist(@Nullable final String email) {
        if (email == null || email.isEmpty()) return false;
        return repository.isEmailExist(email);
    }

    public void lockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final User user = findByLogin(login);
        if (user == null) throw new EntityException(getName());
        user.setLocked(true);
    }

    public void unLockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final User user = findByLogin(login);
        if (user == null) throw new EntityException(getName());
        user.setLocked(false);
    }

}
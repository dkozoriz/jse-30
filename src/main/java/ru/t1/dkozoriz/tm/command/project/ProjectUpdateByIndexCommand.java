package ru.t1.dkozoriz.tm.command.project;

import lombok.Getter;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.util.TerminalUtil;

public final class ProjectUpdateByIndexCommand extends AbstractProjectCommand {

    public ProjectUpdateByIndexCommand() {
        super("project-update-by-index", "update project by index.");
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE PROJECT BY INDEX]");
        @Nullable final String userId = getUserId();
        System.out.println("ENTER INDEX:");
        @Nullable final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER NAME:");
        @Nullable final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @Nullable final String description = TerminalUtil.nextLine();
        getProjectService().updateByIndex(userId, index, name, description);
    }

}

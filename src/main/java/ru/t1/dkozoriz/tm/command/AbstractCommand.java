package ru.t1.dkozoriz.tm.command;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.api.model.ICommand;
import ru.t1.dkozoriz.tm.api.service.IAuthService;
import ru.t1.dkozoriz.tm.api.service.IServiceLocator;
import ru.t1.dkozoriz.tm.enumerated.Role;

@Getter
@Setter
public abstract class AbstractCommand implements ICommand {

    @NotNull
    protected final String name;

    @Nullable
    protected final String description;

    @Nullable
    protected final String argument;

    protected IServiceLocator serviceLocator;

    protected AbstractCommand(@NotNull String name, @Nullable String description) {
        this.name = name;
        this.description = description;
        argument = null;
    }

    protected AbstractCommand(@NotNull String name, @Nullable String description, @Nullable String argument) {
        this.name = name;
        this.description = description;
        this.argument = argument;
    }

    public abstract void execute();

    @NotNull
    public abstract Role[] getRoles();

    @NotNull
    public IAuthService getAuthService() {
        return serviceLocator.getAuthService();
    }

    @Nullable
    public String getUserId() {
        return getAuthService().getUserId();
    }

    @NotNull
    public String toString() {
        final String name = getName();
        final String argument = getArgument();
        final String description = getDescription();
        String result = "";
        final boolean hasName = !name.isEmpty();
        final boolean hasArgument = argument != null && !argument.isEmpty();
        final boolean hasDescription = description != null && !description.isEmpty();
        if (hasName) result += name;
        if (hasArgument) result += hasName ? ", " + argument : argument;
        if (hasDescription) result += ": " + description;
        return result;
    }

}
package ru.t1.dkozoriz.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.util.TerminalUtil;

public final class TaskUpdateByIndexCommand extends AbstractTaskCommand {

    public TaskUpdateByIndexCommand() {
        super("task-update-by-index", "update task by index.");
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE TASK BY INDEX]");
        @Nullable final String userId = getUserId();
        System.out.println("ENTER INDEX:");
        @Nullable final Integer index = TerminalUtil.nextNumber() - 1;
        getTaskService().findByIndex(userId, index);
        System.out.println("ENTER NEW NAME:");
        @Nullable final String name = TerminalUtil.nextLine();
        System.out.println("ENTER NEW DESCRIPTION:");
        @Nullable final String description = TerminalUtil.nextLine();
        getTaskService().updateByIndex(userId, index, name, description);
    }

}

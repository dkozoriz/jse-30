package ru.t1.dkozoriz.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class ApplicationVersionCommand extends AbstractSystemCommand {

    public ApplicationVersionCommand() {
        super("version", "show version info.", "-v");
    }

    public void execute() {
        System.out.println("[VERSION]");
        System.out.println("version: " + getServiceLocator().getPropertyService().getApplicationVersion());
    }

}
package ru.t1.dkozoriz.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class TaskListClearCommand extends AbstractTaskCommand {

    public TaskListClearCommand() {
        super("task-clear", "delete all tasks.");
    }

    @Override
    public void execute() {
        System.out.println("[CLEAR TASKS]");
        @Nullable final String userId = getUserId();
        getTaskService().clear(userId);
    }

}

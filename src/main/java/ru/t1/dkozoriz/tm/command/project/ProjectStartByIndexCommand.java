package ru.t1.dkozoriz.tm.command.project;

import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.enumerated.Status;
import ru.t1.dkozoriz.tm.util.TerminalUtil;

public final class ProjectStartByIndexCommand extends AbstractProjectCommand {

    public ProjectStartByIndexCommand() {
        super("project-start-by-index", "start project by index.");
    }

    @Override
    public void execute() {
        System.out.println("[START PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        @Nullable final Integer index = TerminalUtil.nextNumber() - 1;
        @Nullable final String userId = getUserId();
        getProjectService().changeStatusByIndex(userId, index, Status.IN_PROGRESS);
    }

}

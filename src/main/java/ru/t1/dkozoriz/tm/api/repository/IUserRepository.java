package ru.t1.dkozoriz.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.model.User;

public interface IUserRepository extends IAbstractRepository<User> {

    @Nullable
    User findByLogin(@Nullable String login);

    @Nullable
    User findByEmail(@Nullable String email);

    boolean isLoginExist(@NotNull String login);

    boolean isEmailExist(@NotNull String email);

}
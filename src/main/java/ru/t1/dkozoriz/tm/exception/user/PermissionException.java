package ru.t1.dkozoriz.tm.exception.user;

import ru.t1.dkozoriz.tm.exception.AbstractException;

public final class PermissionException extends AbstractException {

    public PermissionException() {
        super("Error! Permission is incorrect.");
    }

}
